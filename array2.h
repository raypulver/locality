#ifndef ARRAY2_INCLUDED
#define ARRAY2_INCLUDED

#include <stdint.h>

typedef struct _Array2_T {
  void *data;
  uint32_t width;
  uint32_t height;
  int blocksize;
} Array2_T;
extern Array2_T *Array2_new (int width, int height, int);
/* new blocked 2d array: blocksize = square root of # of cells in block */
extern void Array2_free (Array2_T **array2b);
extern int Array2_width (Array2_T *array2b);
extern int Array2_height(Array2_T *array2b);
extern int Array2_size (Array2_T *array2b);
extern int Array2_blocksize (Array2_T *array2b);
extern void *Array2_at(Array2_T *array2b, int i, int j);
/* return a pointer to the cell in column i, row j;
index out of range is a checked run-time error
*/
extern void Array2_map(Array2_T *array2b,
void apply(int i, int j, Array2_T *array2b, void *elem, void *cl), void *cl);
extern void Array2_map_col_major(Array2_T *array2b,
void apply(int i, int j, Array2_T *array2b, void *elem, void *cl), void *cl);
extern void Array2_map_row_major(Array2_T *array2b,
void apply(int i, int j, Array2_T *array2b, void *elem, void *cl), void *cl);
extern void Array2_map_block_major(Array2_T *array2b,
void apply(int i, int j, Array2_T *array2b, void *elem, void *cl), void *cl);
/* visits every cell in one block before moving to another block */
/* it is a checked run-time error to pass a NULL T
to any function in this interface */
#endif
