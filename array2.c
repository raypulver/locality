#include "array2.h"
#include <stdlib.h>

Array2_T *Array2_new(int width, int height, int size) {
  Array2_T *retval = malloc(sizeof(Array2_T));
  retval->width = width;
  retval->height = height;
  retval->blocksize = size;
  retval->data = malloc(width*height*size);
  return retval;
}

void Array2_free (Array2_T **array2b) {
  free((*array2b)->data);
  free(*array2b);
  *array2b = NULL;
}

int Array2_width(Array2_T *array2b) {
  return (int) array2b->width;
}

int Array2_height(Array2_T *array2b) {
  return (int) array2b->height;
}

int Array2_size (Array2_T *array2b) {
  return (int) array2b->width * (int) array2b->height;
}

int Array2_blocksize (Array2_T *array2b) {
  return (int) array2b->blocksize;
}
void *Array2_at(Array2_T *array2b, int i, int j) {
  return (void *) (((char *) array2b->data) + (j * array2b->width + i)*array2b->blocksize);
}

void Array2_map_col_major(Array2_T *array2b, void apply(int, int, Array2_T *, void *, void *), void *cl) {
  for (int x = 0; x < (int) array2b->width; ++x) {
    for (int y = 0; y < (int) array2b->height; ++y) {
      apply(x, y, array2b, Array2_at(array2b, x, y), cl);
    }
  }
}

void Array2_map_row_major(Array2_T *array2b, void apply(int, int, Array2_T *, void *, void *), void *cl) {
  for (int y = 0; y < (int) array2b->height; ++y) {
    for (int x = 0; x < (int) array2b->width; ++x) {
      apply(x, y, array2b, Array2_at(array2b, x, y), cl);
    }
  }
}

void Array2_map(Array2_T *array2b, void apply(int, int, Array2_T *, void *, void *), void *cl) {
  Array2_map_row_major(array2b, apply, cl);
}

void Array2_map_block_major(Array2_T *array2b, void apply(int, int, Array2_T *, void *, void *), void *cl) {
  Array2_map_row_major(array2b, apply, cl);
}
