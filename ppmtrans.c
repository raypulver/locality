#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "assert.h"
#include "a2methods.h"
#include "a2plain.h"
#include "a2blocked.h"
#include "pnm.h"

typedef struct rotation_context_t {
  int blocksize;
  int height;
  int width;
  int size;
  Pnm_ppm ppm;
  A2Methods_T methods;
} rotation_context_t;

static void output_image(Pnm_ppm img) {
  Pnm_ppmwrite(stdout, img);
}

/*
static void map_to_transpose(int i, int j, A2Methods_Array2 ary, void *pixel, void *cl) {
  (void) ary;
  rotation_context_t *ctx = cl;
  memcpy(ctx->methods->at(ctx->ppm->pixels, j, i), pixel, ctx->size);
}
*/

static void map_to_90(int i, int j, A2Methods_Array2 ary, void *pixel, void *cl) {
  (void) ary;
  rotation_context_t *ctx = cl;
  memcpy(ctx->methods->at(ctx->ppm->pixels, ctx->height - j - 1, i), pixel, ctx->size);
}

static void map_to_180(int i, int j, A2Methods_Array2 ary, void *pixel, void *cl) {
  (void) ary;
  rotation_context_t *ctx = cl;
  memcpy(ctx->methods->at(ctx->ppm->pixels, ctx->width - i - 1, ctx->height - j - 1), pixel, ctx->size);
}

static void map_to_270(int i, int j, A2Methods_Array2 ary, void *pixel, void *cl) {
  (void) ary;
  rotation_context_t *ctx = cl;
  memcpy(ctx->methods->at(ctx->ppm->pixels, ctx->height - j - 1, ctx->width - i - 1), pixel, ctx->size);
}

int main(int argc, char *argv[]) {
  int rotation = 0;
  A2Methods_T methods = array2_methods_plain; // default to Array2 methods
  assert(methods);
  A2Methods_mapfun *map = methods->map_default; // default to best map
  assert(map);
#define SET_METHODS(METHODS, MAP, WHAT) do { \
      methods = (METHODS); \
      assert(methods); \
      map = methods->MAP; \
      if (!map) { \
        fprintf(stderr, "%s does not support " WHAT "mapping\n", argv[0]); \
        exit(1); \
      } \
    } while(0)

  int i;
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-row-major")) {
      SET_METHODS(array2_methods_plain, map_row_major, "row-major");
    } else if (!strcmp(argv[i], "-col-major")) {
      SET_METHODS(array2_methods_plain, map_col_major, "column-major");
    } else if (!strcmp(argv[i], "-block-major")) {
      SET_METHODS(array2_methods_blocked, map_block_major, "block-major");
    } else if (!strcmp(argv[i], "-rotate")) {
      assert(i + 1 < argc);
      char *endptr;
      rotation = strtol(argv[++i], &endptr, 10);
      assert(*endptr == '\0'); // parsed all correctly
      assert(rotation == 0   || rotation == 90
          || rotation == 180 || rotation == 270);
    } else if (*argv[i] == '-') {
      fprintf(stderr, "%s: unknown option '%s'\n", argv[0], argv[i]);
      exit(1);
    } else if (argc - i > 2) {
      fprintf(stderr, "Usage: %s [-rotate <angle>] "
              "[-{row,col,block}-major] [filename]\n", argv[0]);
      exit(1);
    } else {
      break;
    }
  }
  Pnm_ppm ppm = Pnm_ppmread(stdin, methods);
  Pnm_ppm ppmcopy = malloc(sizeof(*ppm));
  memcpy(ppmcopy, ppm, sizeof(*ppm));
  ppmcopy->pixels = NULL;
  A2Methods_Array2 pixels = ppm->pixels;
  A2Methods_Array2 pixelscopy;
  rotation_context_t cl;
  cl.ppm = ppmcopy;
  cl.blocksize = methods->blocksize(pixels);
  cl.size = methods->size(pixels);
  printf("%i\n", cl.size);
  cl.methods = methods;
  cl.width = methods->width(pixels);
  cl.height = methods->height(pixels);
  int tmp;
  switch (rotation) {
    case 0:
      output_image(ppm);
      break;
    case 90:
      tmp = ppmcopy->height;
      ppmcopy->height = ppmcopy->width;
      ppmcopy->width = tmp;
      pixelscopy = methods->new_with_blocksize(methods->height(pixels), methods->width(pixels), methods->size(pixels), methods->blocksize(pixels));
      ppmcopy->pixels = pixelscopy;
      map(pixels, &map_to_90, &cl);
      output_image(ppmcopy);
      break;
    case 180: 
      pixelscopy = methods->new_with_blocksize(methods->width(pixels), methods->height(pixels), methods->size(pixels), methods->blocksize(pixels));
      ppmcopy->pixels = pixelscopy;
      map(pixels, &map_to_180, &cl);
      output_image(ppmcopy);
      break;
    case 270:
      pixelscopy = methods->new_with_blocksize(methods->height(pixels), methods->width(pixels), methods->size(pixels), methods->blocksize(pixels));
      tmp = ppmcopy->height;
      ppmcopy->height = ppmcopy->width;
      ppmcopy->width = tmp;
      ppmcopy->pixels = pixelscopy;
      map(pixels, &map_to_270, &cl);
      output_image(ppmcopy);
      break;
  }
  Pnm_ppmfree(&ppm);
  if (ppmcopy->pixels) Pnm_ppmfree(&ppmcopy);
  else free(ppmcopy);
}
