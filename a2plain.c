#include <stdlib.h>

#include <a2plain.h>
#include "array2.h"

// define a private version of each function in A2Methods_T that we implement
typedef A2Methods_Array2 A2;

static A2 new(int width, int height, int size) {
  return Array2_new(width, height, size);
}

static A2 new_with_blocksize(int width, int height, int size,
                                        int blocksize)
{
  (void) blocksize;
  return Array2_new(width, height, size);
}

static void a2free (A2 *array2) {
  Array2_free((Array2_T **)array2);
}


static int width    (A2 array2) { return Array2_width(array2); }
static int height   (A2 array2) { return Array2_height(array2); }
static int size     (A2 array2) { return Array2_size(array2); }
static int blocksize(A2 array2) { return Array2_blocksize(array2); }

// now create the private struct containing pointers to the functions
static A2Methods_Object *at(A2 array2, int i, int j) {
  return Array2_at(array2, i, j);
}

typedef void applyfun(int i, int j, Array2_T *array2b, void *elem, void *cl);

static void map_col_major (A2 array2, A2Methods_applyfun apply, void *cl) {
  Array2_map_col_major(array2, (applyfun*)apply, cl);
}

static void map_row_major (A2 array2, A2Methods_applyfun apply, void *cl) {
  Array2_map_row_major(array2, (applyfun*)apply, cl);
}

/*
static void map_block_major (A2 array2, A2Methods_applyfun apply, void *cl) {
  Array2_map_block_major(array2, (applyfun*)apply, cl);
}
*/

static struct A2Methods_T array2_methods_plain_struct = {
  new,
  new_with_blocksize,
  a2free,
  width,
  height,
  size,
  blocksize,
  at,
  map_row_major, // map_row_major
  map_col_major, // map_col_major
  NULL,
  map_row_major, // map_default
};

A2Methods_T array2_methods_plain = &array2_methods_plain_struct;
