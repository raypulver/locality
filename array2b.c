#include <stdlib.h>
#include <stdint.h>
#include <sys/param.h>
#include <stdio.h>
#include "array2b.h"

Array2b_T *Array2b_new (int width, int height, int size, int blocksize) {
  Array2b_T *retval = malloc(sizeof(*retval));
  retval->data = malloc(width*height*size);
  retval->blocksize = blocksize;
  retval->size = size;
  retval->width = width;
  retval->height = height;
  return retval;
}
  
Array2b_T *Array2b_new_64K_block(int width, int height, int size) {
  return Array2b_new(width, height, size, 64*1024);
}

void Array2b_free (Array2b_T **array2b) {
  free((*array2b)->data);
  free(*array2b);
  *array2b = NULL;
}

int Array2b_width (Array2b_T *ary) {
  return ary->width;
}

int Array2b_height(Array2b_T *ary) {
  return ary->height;
}

int Array2b_size (Array2b_T *ary) {
  return ary->size;
}

int Array2b_blocksize(Array2b_T *ary) {
  return ary->blocksize;
}

size_t Array2b_offset_from_x_y(Array2b_T *ary, int i, int j) {
  size_t offset_from_blocks_above = (j*ary->size - (j*ary->size) % (ary->blocksize))*ary->width;
  size_t offset_from_blocks_left = (i*ary->size - (i*ary->size) % ary->blocksize)*MIN(ary->blocksize, ary->height*ary->size - j*ary->size);
  size_t vertical_offset_current_block = (j*ary->size % ary->blocksize)*MIN(ary->blocksize*ary->size, ary->width*ary->size - i*ary->size + 1);
  size_t horizontal_offset_current_block = i*ary->size % ary->blocksize;
  return offset_from_blocks_above + offset_from_blocks_left + vertical_offset_current_block + horizontal_offset_current_block;
}


void *Array2b_at(Array2b_T *ary, int i, int j) {
  uint8_t *ptr = ary->data;
  return (void *) (ptr + Array2b_offset_from_x_y(ary, i, j));
}
  
void Array2b_map(Array2b_T *ary, 
void apply(int i, int j, Array2b_T *ary, void *elem, void *cl), void *cl) {
  for (int px = 0; px < (int) ary->width; px += ary->blocksize) {
    for (int py = 0; py < (int) ary->height; px += ary->blocksize) {
      for (int x = 0; x < (int) MIN(ary->blocksize/ary->size, ary->width - px); ++x) {
        for (int y = 0; y < (int) MIN(ary->blocksize/ary->size, ary->height - py); ++y) {
          apply(px + x, py + y, ary, Array2b_at(ary, px + x, py + y), cl);
        }
      }
    }
  }
}
